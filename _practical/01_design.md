---
slug: design
title: Project Design & Planning
---

# Step 1: Initial Challenge

You are assigned to manage profile pictures of everyone on your team of 13 people, for use on your organization's website.  You have some, but not all of the pictures.  Some of the pictures are old.  They are in several different formats, are different sizes, and so on.  Generally, a mess.

 - Write down the requirements for this assignment.  Note: the description above may be incomplete when it comes to writing down all the requirements!  You may need to ask the faculty specific questions to figure out the rest of the requirements.
 - Draw a design diagram for a way to satisfy (programmatically) those requirements
 - Translate that diagram into high level pseudo-code of the process

# Step 2: Improvements!

Your supervisor is very pleased with how you have been managing the team's profile pictures on the website, and bragged about it to other managers.  Now you've been assigned to manage the profile pictures for everyone in the company (roughly 500 people), and the CEO is really looking forward to seeing the revamped site.  Your group's pictures are now cleaned up, but once again you have a big mess of profile picture files, and new requirements.

 - This time, extend the requirements you previously identified with any additions.  Note: the description above may be incomplete when it comes to writing down all the requirements!  You may need to ask the faculty specific questions to figure out the rest of the requirements.
 - Draw a new design diagram to satisfy the new requirements, but highlight the pieces that are preserved from your original diagram
 - Similarly, update your pseudo code for processing all the input, again noting what was preserved from your original process

# Step 3: Teamwork!

Now stuck managing all these profile pictures, you take to commiserating with other fellow over-acheivers in other companies.

 - Get into groups of 3, and compare your list of requirements from steps 1 & 2.  What are the overlaps?  What are the differences?
 - What differences did you get in diagrams? Did different approaches in step 1 lead to different amounts of change to satisfy the requirements in step 2?
 - What differences did you get in pseudo code?  How do differences in the diagrams show up (or not) as differences in the pseudo code?

# Step 4: Optional given time

You and your new friends decide to start a company for managing profile pictures, for other companies.

 - In your same group of 3, decide what the new requirements for your company's product are.
 - Draw a new diagram & write new pseudo-code
 - In negotiating the new requirements and new design as a team, what was different from doing the work on your own?